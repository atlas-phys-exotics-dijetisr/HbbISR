#!/bin/bash

export PACKAGE=HbbISR
source ${AnalysisBase_PLATFORM}/bin/librun.sh

# sample list
MCA_GRID="../HbbISR/filelists/mc16a/Pythia8_dijet.fatjet.NTUP.list"
MCC_GRID="../HbbISR/filelists/mc16c/Pythia8_dijet.fatjet.NTUP.list"
DATA_GRID="../HbbISR/filelists/data15.fatjet.NTUP.list ../HbbISR/filelists/data16.fatjet.NTUP.list ../HbbISR/filelists/data17.fatjet.NTUP.list"

# Process
RUNLIST=""

# fatjet
runOne fatjettrigger fatjettrigger_mca_test "-m" ${MCA_GRID}
runOne fatjettrigger fatjettrigger_mcc_test "-m" ${MCC_GRID}
runOne fatjettrigger fatjettrigger_data_test "" ${DATA_GRID}

for RUN in ${RUNLIST}
do
    echo "Waiting for PID ${RUN}"
    wait ${RUN} || exit 1
    echo "COMPLETED WITH ${?}"
done

#
# Merge
#

# fatjet
merge fatjettrigger_mca_test hist-qcd.root "hist-*Pythia*jetjet*JZ*[0-9]W.*.root"

merge fatjettrigger_mcc_test hist-qcd.root "hist-*Pythia*jetjet*JZ*[0-9]W.*.root"

merge fatjettrigger_data_test hist-data15.root "hist-*data15*root"
merge fatjettrigger_data_test hist-data16.root "hist-*data16*root"
merge fatjettrigger_data_test hist-data17.root "hist-*data17*root"
