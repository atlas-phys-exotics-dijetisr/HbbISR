#!/usr/bin/env python

import sys
import time
import tempfile
import getpass
import subprocess
import os
import argparse

from ZprimeNtupler import ntupler

##-----------------------------------------------##
## Users: set sample information here

#
# Mapping of different options to run. Each option has
# the following attributes:
#  selection: name of ntupler configuration to run (SELECTION from config_SELECTION_ntuple.py)
#  filelists_type: list of filelists (FILELIST from FILELIST.deriv.list) for type
#                  type can be any one of data, bkg, sig or fastsim
#  derivation: name of derivation to use for the filelists (DERIV from filelist.DERIV.list)
#  
optionDict = {
    "fatjet" :  {
        'selection': 'fatjet',
        'derivation':'EXOT8',
        'filelists_bkg':[
            "Dijet_Pythia8",
            "Dijet_Sherpa"
        ],
        'filelists_sig':[
            "TTbar_Pythia8",
            "TTbar_Herwig",
            "TTbar_Sherpa",
            "Powheg_singletop",
            "Sherpa_Vqq",
            "Herwig_Vqq",
            "Diboson",
            "Higgs_Pythia8",
            "Higgs_Herwig",
            "Signal_trijet",
            "Sherpa_Wlnu"
        ],
        'filelists_fastsim':[
            "TTbar_Pythia8_FastSim",
            "TTbar_Herwig_FastSim",
            "Dijet_Sherpa_FastSim"
        ],
        'filelists_data':["data15",
                          "data16",
                          "data17",
                          "data18"
        ]
    },
    "fatjet_truth" :  {
        'selection': 'fatjet_truth',
        'derivation':'TRUTH3',
        'filelists_bkg':[
            "Sherpa_Vqq"
            "Higgs_Pythia8"
            "TTbar_Pythia8",
            "TTbar_Herwig"
        ]
    },
}

## End of user specified information.

#### RUN EVERYTHING ####
ntupler.runntupler(optionDict,package='HbbISR')
