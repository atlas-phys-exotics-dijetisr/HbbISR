#!/bin/bash

export PACKAGE=HbbISR
source ${AnalysisBase_PLATFORM}/bin/librun.sh

# sample list
SIG_GRID="../HbbISR/filelists/mc16?/Signal_trijet.fatjet.NTUP.list ../HbbISR/filelists/mc16?/Higgs_ggFHbb.fatjet.NTUP.list ../HbbISR/filelists/mc16?/Higgs_vbfHbb.fatjet.NTUP.list ../HbbISR/filelists/mc16?/Higgs_VHbb.fatjet.NTUP.list ../HbbISR/filelists/mc16?/Sherpa_?qq.fatjet.NTUP.list ../HbbISR/filelists/mc16?/Powheg_ttbar.fatjet.NTUP.list ../HbbISR/filelists/mc16?/Powheg_singletop.fatjet.NTUP.list"
BKG_GRID="../HbbISR/filelists/mc16?/Pythia8_dijet.fatjet.NTUP.list ../HbbISR/filelists/mc16?/Herwig_?qq.fatjet.NTUP.list ../HbbISR/filelists/mc16?/Sherpa22_?qq.fatjet.NTUP.list ../HbbISR/filelists/mc16?/Sherpa_ttbar.fatjet.NTUP.list ../HbbISR/filelists/mc16?/Sherpa_W*nu.fatjet.NTUP.list"
DATA_GRID="../HbbISR/filelists/data15.fatjet.NTUP.list ../HbbISR/filelists/data16.fatjet.NTUP.list ../HbbISR/filelists/data17.fatjet.NTUP.list"

syslist=(JET_Comb_Baseline_All JET_Comb_Modelling_All JET_Comb_TotalStat_All JET_Comb_Tracking_All)
varlist=(1up 2up 3up 1down 2down 3down)
combsyslist=()

# Process
RUNLIST=""

# fatleadjet
runOne fatleadjet fatleadjet_mc_test_nominal "-m" ${SIG_GRID} ${BKG_GRID}
for sys in ${syslist[@]}
do
    for var in ${varlist[@]}
    do
  	sysname="${sys}__${var}"
  	runOne fatleadjet fatleadjet_mc_test_sys${sysname} "-m -s ${sysname}" ${SIG_GRID}
 	combsyslist+=(${sysname})
    done
done

runOne fatleadjet fatleadjet_data_test "" ${DATA_GRID}

echo "Waiting..."
wait

#
# Merge
#

# fatleadjet
mergeSys fatleadjet_mc_test ${combsyslist[@]}
mergemc fatleadjet_mc_test qcd.root "hist-*Pythia*jetjet*JZ*[0-9]W.*.root"
mergemc fatleadjet_mc_test ttbar.root "hist-*PhPy8EG_A14_ttbar_hdamp258p75*.root"
mergemc fatleadjet_mc_test ttbar-Sherpa.root "hist-*Sherpa_221_NNPDF30NNLO_ttbar*.root"
mergemc fatleadjet_mc_test singletop.root "hist-*PowhegPythia8EvtGen_A14_Wt*.root" #"hist-*PhPy8EG_A14_tchan*.root"
mergemc fatleadjet_mc_test w.root "hist-*Sherpa_CT10_Wqq*.root"
mergemc fatleadjet_mc_test z.root "hist-*Sherpa_CT10_Zqq*.root"
mergemc fatleadjet_mc_test v.root "hist-*Sherpa_CT10_*qq*.root"
mergemc fatleadjet_mc_test w-Herwig.root "hist-*Herwigpp_UEEE5CTEQ6L1_Wj*.root"
mergemc fatleadjet_mc_test z-Herwig.root "hist-*Herwigpp_UEEE5CTEQ6L1_Zj*.root"
mergemc fatleadjet_mc_test v-Herwig.root "hist-*Herwigpp_UEEE5CTEQ6L1_*j*.root"
mergemc fatleadjet_mc_test w-Sherpa225.root "hist-*Sherpa_225_NNPDF30NNLO_Wqq*.root"
mergemc fatleadjet_mc_test z-Sherpa225.root "hist-*Sherpa_225_NNPDF30NNLO_Zqq*.root"
mergemc fatleadjet_mc_test v-Sherpa225.root "hist-*Sherpa_225_NNPDF30NNLO_*qq*.root"
mergemc fatleadjet_mc_test sig.root "hist-*PowhegPy8EG_NNLOPS_nnlo_30_ggH125_bb_kt200*.root" "hist-*345931*.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_bb*.root" "hist-*.Pythia8EvtGen_A14NNPDF23LO_*H125_bb_fj350*.root"
mergemc fatleadjet_mc_test sig-nofilt.root "hist-*345342.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_bb*.root" "hist-*.345338.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_bb*.root" "hist-*.34228?.Pythia8EvtGen_A14NNPDF23LO_*H125_inc*.root" 
for i in mRp1 mRp125 mRp15 mRp175 mRp2 mRp25 mRp3
do
    mergemc fatleadjet_mc_test mc16_13TeV.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350P100_${i}_gSp25.root "hist-user.kkrizka.mc16_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_*j_Jet350P100_${i}_gSp25.*_tree.root.root"
done 
mergemc fatleadjet_mc_test Wlnu.root "hist-*_W*nu*.root"
mergemc fatleadjet_mc_test Wenu.root   "hist-*Wenu*.root"
mergemc fatleadjet_mc_test Wmunu.root  "hist-*Wmunu*.root"
mergemc fatleadjet_mc_test Wtaunu.root "hist-*Wtaunu*.root"

merge fatleadjet_data_test data15.root "hist-*data15*root"
merge fatleadjet_data_test data16.root "hist-*data16*root"
merge fatleadjet_data_test data17.root "hist-*data17*root"
merge fatleadjet_data_test data.root "hist-data*.root"
merge fatleadjet_data_test data1516.root "hist-data1[56]*.root"
