import ROOT
from ZprimeNtupler import Config, utils
from ZprimeNtupler import commonconfig

c = Config()

jetDetailStr="kinematic clean"
if args.is_MC: jetDetailStr+=" truth"

fatjetDetailStr="kinematic substructure constituent trackJetName:GhostVR30Rmax4Rmin02TrackJet_BTagging201903 trackJetPtCut:5.0 muonCorrection hbbtag:2020v3"
if args.is_MC: fatjetDetailStr+=" bosonCount truth"
fatjetDetailStrSyst="kinematic trackJetName:GhostVR30Rmax4Rmin02TrackJet_BTagging201903 trackJetPtCut:5.0 muonCorrection hbbtag"

subjetDetailStr   ="kinematic flavorTag constituent %s"%(commonconfig.generate_btag_detailstr(btaggers=['DL1','DL1r']))

muonDetailStr="kinematic isolationKinematics trigger"
elDetailStr="kinematic"

muonTriggers=[ # muon triggers
    'HLT_mu20_iloose_L1MU15','HLT_mu40','HLT_mu60_0eta105_msonly', #2015
    'HLT_mu26_ivarmedium','HLT_mu50', #2016
    'HLT_mu26_ivarmedium','HLT_mu50','HLT_mu60_0eta105_msonly', #2017
    'HLT_mu26_ivarmedium','HLT_mu50'] #2018
muonTriggers=set(muonTriggers)

triggerList=['HLT_j110',
             'HLT_j260_a10_lcw_L1J75','HLT_j260_a10_lcw_subjes_L1J100',
             'HLT_j260_a10r_L1J75','HLT_j260_a10r_L1J100',
             'HLT_j260_a10t_lcw_jes_L1J75','HLT_j260_a10t_lcw_jes_L1J100'] # Reference triggers
triggerList+=['HLT_j360',
              'HLT_j360_a10_lcw_sub_L1J100',
              'HLT_j360_a10r_L1J100'] # 2015 triggers
triggerList+=['HLT_j360_a10_lcw_L1J100','HLT_j400_a10_lcw_L1J100','HLT_j420_a10_lcw_L1J100',
              'HLT_j360_a10r_L1J100','HLT_j400_a10r_L1J100','HLT_j420_a10r_L1J100'] # 2016 triggers
triggerList+=['HLT_j420_a10t_lcw_jes_L1J100','HLT_j440_a10t_lcw_jes_L1J100','HLT_j460_a10t_lcw_jes_L1J100','HLT_j480_a10t_lcw_jes_L1J100',
              'HLT_j390_a10t_lcw_jes_30smcINF_L1J100','HLT_j390_a10t_lcw_jes_30smcINF_L1J100','HLT_j420_a10t_lcw_jes_40smcINF_L1J100','HLT_j440_a10t_lcw_jes_40smcINF_L1J100',
              'HLT_j420_a10_lcw_subjes_L1J100','HLT_j440_a10_lcw_subjes_L1J100','HLT_j460_a10_lcw_subjes_L1J100','HLT_j480_a10_lcw_subjes_L1J100',
              'HLT_j420_a10r_L1J100','HLT_j440_a10r_L1J100','HLT_j460_a10r_L1J100','HLT_j480_a10r_L1J100',
              'HLT_j460_a10_lcw_subjes_L1J100'] # 2017 triggers
triggerList+=['HLT_j460_a10r_L1SC111','HLT_j460_a10r_L1J100','HLT_j460_a10_lcw_subjes_L1SC111','HLT_j460_a10_lcw_subjes_L1J100','HLT_j460_a10t_lcw_jes_L1SC111','HLT_j460_a10t_lcw_jes_L1J100',
              'HLT_j420_a10t_lcw_jes_30smcINF_L1J100','HLT_j420_a10t_lcw_jes_30smcINF_L1SC111','HLT_j420_a10t_lcw_jes_35smcINF_L1J100','HLT_j420_a10t_lcw_jes_35smcINF_L1SC111'] # 2018 triggers
triggerList+=muonTriggers
triggerList=set(triggerList)

commonconfig.apply_common_config(c,
                                 isMC=args.is_MC,isAFII=args.is_AFII,
                                 triggerSelection='|'.join(triggerList),
                                 triggerList=','.join(triggerList),
                                 GRLset='ALLGOOD',
                                 doJets=False,doFatJets=True,doTrackJets=True,doPhotons=False,doMuons=True,doElectrons=False,
                                 doTruthFatJets=args.is_MC,
                                 fatjetMuonInJet=True,
                                 trkJetBtaggers =['DL1','DL1r'],
                                 trkJetBtagmodes=commonconfig.btagmodes,
                                 trkJetBtagWPs  =commonconfig.btagWPs,
                                 trkJetBTS      ='201903',
                                 singleMuTriggers=','.join(muonTriggers),
                                 args=args.extra_options)

#
# Systematics modifications
if args.is_MC:
    CalibrateFatJets=commonconfig.findAlgo(c,"CalibrateFatJets")
    CalibrateFatJets.m_uncertConfig="HbbISR/R10_CategoryReduction.config"

#
# Selection
if args.is_MC:
    SelectFatJets=commonconfig.findAlgo(c,"SelectFatJets")
    SelectFatJets.m_pass_min = 1

#
# Add Hbb tag tool
c.algorithm("Zprime::HbbTagAlgorithm", {
    "HbbTagTool" : {
        "Tool"  : "FlavorTagDiscriminants::HbbTagTool",
        "nnFile": "dev/BTagging/2020v3/Xbb/GhostVR30Rmax4Rmin02TrackJet_BTagging201903/network.json"
    },
    "InputAlgo"           : "SignalFatJets_Algo",
    "InContainerName"     : "SignalFatJets"
})

#
# Define output containers
containers=[]

containers.append('FatJet(SignalFatJets->fatjet);'+fatjetDetailStr+';'+fatjetDetailStrSyst)
containers.append('Jet(AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903->trkjet);'+subjetDetailStr+';')
containers.append('Muon(SignalMuons->muon);'+muonDetailStr+';kinematic')

if args.is_MC:
    SelectTruthFatJets=commonconfig.findAlgo(c,"SelectTruthFatJets")
    SelectTruthFatJets.m_pT_min =150e3
    SelectTruthFatJets.m_eta_max=2.5

    containers.append('FatJet(SignalTruthFatJets->truthfatjet);kinematic;')

c.output('ANALYSIS')
c.algorithm("Zprime::Ntupler", {
    "PMGCrossSectionTool" : {
        "Tool" : "PMGTools::PMGCrossSectionTool",
    },
    "InputAlgo"           : "SignalFatJets_Algo",
    "DijetFatjetSlicing"  : False,
    "Containers"          : containers,
    "EventDetailStr"      : "eventCleaning pileup weightsSys",
    "TrigDetailStr"       : "passTrigBits passTriggers triggerList:{}".format(','.join(triggerList))
} )
