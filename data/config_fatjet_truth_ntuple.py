import ROOT
from ZprimeNtupler import Config

msgLevel=ROOT.MSG.INFO

c = Config()

c.anaalgorithm("Zprime::BasicEventSelection", {
    "TruthLevelOnly"      : True,
    "UseMetaData"         : False
} )

c.anaalgorithm("Zprime::SortAlgo", {
    "InContainerName"  : "AntiKt10TruthTrimmedPtFrac5SmallR20Jets",
    "OutContainerName" : "AntiKt10TruthTrimmedPtFrac5SmallR20JetsSort"
} )

c.anaalgorithm("Zprime::SortAlgo", {
    "InContainerName"  : "AntiKtVR30Rmax4Rmin02TruthChargedJets",
    "OutContainerName" : "AntiKtVR30Rmax4Rmin02TruthChargedJetsSort"
} )

c.algorithm("JetSelector",                  { "m_inContainerName"         : "AntiKt10TruthTrimmedPtFrac5SmallR20JetsSort",
                                              "m_outContainerName"        : "SignalFatJets",
                                              "m_outputAlgo"              : "SignalFatJetsAlgo",
                                              "m_useCutFlow"              : False,
                                              "m_pT_min"                  : 250e3,
                                              "m_eta_max"                 : 2.0
} )

c.algorithm("JetSelector",                  { "m_inContainerName"         : "AntiKtVR30Rmax4Rmin02TruthChargedJetsSort",
                                              "m_outContainerName"        : "SignalTrackJets",
                                              "m_outputAlgo"              : "SignalTrackJetsAlgo",
                                              "m_useCutFlow"              : False,
                                              "m_pT_min"                  : 10e3,
                                              "m_eta_max"                 : 2.5
} )

containers=[]
containers.append('FatJet(SignalFatJets->fatjet);kinematic trackJetName:GhostAntiKtVR30Rmax4Rmin02TruthChargedJets;')
containers.append('Jet(SignalTrackJets->trkjet);kinematic flavorTag;')

c.output('ANALYSIS')
c.algorithm("Zprime::Ntupler", {
    "PMGCrossSectionTool" : {
        "Tool" : "PMGTools::PMGCrossSectionTool",
    },
    "TruthLevelOnly" : True,
    "EventDetailStr" : "weightsSys",
    "Containers"     : containers
} )
